export default function (req, res, next) {
  // find the redirect if it exists where the from === the requested url
  const redirect = (req.url === '/')

  // If it exists, redirect the page with a 301 response else carry on
  if (redirect) {
    res.writeHead(301, { Location: '/inbox' })
    res.end()
  } else {
    next()
  }
}
